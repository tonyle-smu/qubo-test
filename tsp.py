import os
from pyqubo import Array, Placeholder, solve_qubo, Constraint, Sum
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np

from tsp_parser import produce_final


A = 4.0
B = 10.0


SCALE_SIZE = 40


class TSPDataParser(object):
    @staticmethod
    def read_data(file_path):
        data = produce_final(file_path)
        cities = [City(str(_[0]), _[1]) for _ in data]
        return cities


class City(object):
    def __init__(self, name, position):
        self.name = name
        self.x = float(position[0]) / SCALE_SIZE
        self.y = float(position[1]) / SCALE_SIZE

    def distance(self, city):
        return np.sqrt((self.x - city.x) ** 2 + (self.y - city.y) ** 2)

    @property
    def position(self):
        return self.x, self.y


class Solution(object):
    @staticmethod
    def parse_raw_solution(raw_solution, cities):
        solution, distance = [], 0
        previous_city, start_city = None, None
        for t in raw_solution:
            for city_index in raw_solution[t]:
                if raw_solution[t][city_index] == 1:
                    city = cities[city_index]
                    if previous_city:
                        distance += previous_city.distance(city)

                    if previous_city is None:
                        start_city = city

                    previous_city = city

                    solution.append((city.name, city))
                    break

        distance += previous_city.distance(start_city)
        return solution, distance


class Graph(object):
    def __init__(self, cities):
        self.city_count = cities.__len__()
        self.cities = cities
        self.solution = None

    @property
    def city_dict(self):
        return {c.name: c.position for c in self.cities}

    def plot_path(self, nx_graph):
        for i in range(self.city_count):
            city_source = self.solution[i][1]
            city_destination = self.solution[(i + 1) % self.city_count][1]
            nx_graph.add_edge(city_source.name, city_destination.name)
        return nx_graph

    def plot(self):
        nx_graph = nx.Graph()
        for city in self.cities:
            nx_graph.add_node(city.name)

        if self.solution:
            nx_graph = self.plot_path(nx_graph)

        plt.figure(figsize=(5, 5))
        nx.draw_networkx(nx_graph, self.city_dict)
        plt.axis("off")
        plt.show()


class TSPSolver(object):
    def __init__(self, cities):
        self.city_count = cities.__len__()
        self.cities = cities

    def distance(self, i, j):
        return self.cities[i].distance(self.cities[j])

    def time_constraint(self, x):
        time_const = 0.0
        for i in range(self.city_count):
            # If you wrap the hamiltonian by Const(...), this part is recognized as constraint
            time_const += Constraint(
                (Sum(0, self.city_count, lambda j: x[i, j]) - 1) ** 2,
                label="time{}".format(i))
        return time_const

    def city_constraint(self, x):
        # Constraint not to visit the same city more than twice.
        city_const = 0.0
        for j in range(self.city_count):
            city_const += Constraint(
                (Sum(0, self.city_count, lambda i: x[i, j]) - 1) ** 2,
                label="city{}".format(j))
        return city_const

    def related_constraint(self, x):
        related_const = 0.0
        for i in range(self.city_count):
            for j in range(self.city_count):
                for k in range(self.city_count):
                    related_const += Constraint(
                        x[k, i] * x[(k + 1) % self.city_count, j],
                        label='related{}.{}'.format(i, j))
        return related_const

    def target_func(self, x):
        distance = 0.0
        for i in range(self.city_count):
            for j in range(self.city_count):
                for k in range(self.city_count):  # time
                    d_ij = self.distance(i, j)
                    # x[k, i] visit i at time k
                    # x[(k+1)%n_city, j] visit j at time (k+1) % n
                    distance += d_ij * x[k, i] * x[(k + 1) % self.city_count, j]
        return distance

    def build_model(self):
        x = Array.create('x', (self.city_count, self.city_count), 'BINARY')
        A, B = Placeholder("A"), Placeholder("B")
        HA = A * (self.time_constraint(x) + self.city_constraint(x))
        HB = B * self.target_func(x)
        H = HA + HB
        return H

    def compile(self):
        model = self.build_model()
        return model.compile()

    def solve(self, feed_dict={}):
        model = self.compile()
        qubo, offset = model.to_qubo(feed_dict=feed_dict)
        tmp_solution = solve_qubo(qubo)
        decoded_solution, broken, energy = model.decode_solution(tmp_solution, vartype="BINARY", feed_dict=feed_dict)

        if broken.__len__() > 0:
            raise Exception("Number of broken constarint = {}".format(len(broken)))

        result = Solution.parse_raw_solution(decoded_solution["x"], self.cities)
        return result


file_path = os.path.join('data', 'test.tsp')
cities = TSPDataParser.read_data(file_path)

draw_graph = True

if draw_graph:
    graph = Graph(cities)

    feed_dict = dict(A=A, B=B)
    graph.solution, distance = TSPSolver(cities).solve(feed_dict)

    print('Total distance {}'.format(distance))
    graph.plot()

else:
    feed_dict = dict(A=A, B=B)
    solution, distance = TSPSolver(cities).solve(feed_dict)

    print('Total distance {}'.format(distance))



